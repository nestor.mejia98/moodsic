# Asignatura: Inteligencia Artificial (IYA051)
# Grado en Ingeniería Informática
# Escuela Politécnica Superior
# Universidad Europea del Atlántico

# Caso Práctico (ML_Clasificación_CNN_02)
# En esta práctica se crea una red neuronal utilizando convolución
# Para entrenar una base de imágenes de perros y gatos

# Librerías
import matplotlib.pyplot as plt
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
import os
import shutil
import numpy as np

# Comprobar que el número de imágenes es correcto
# print('total training cat images:', len(os.listdir(train_cats_dir)))

# print('total training dog images:', len(os.listdir(train_dogs_dir)))

# print('total validation cat images:', len(os.listdir(validation_cats_dir)))

# print('total validation dog images:', len(os.listdir(validation_dogs_dir)))

# print('total test cat images:', len(os.listdir(test_cats_dir)))

# print('total test dog images:', len(os.listdir(test_dogs_dir)))

# Construir la red neuronal de convolución

from keras import layers
from keras import models
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(40, 40, 3)))
model.add(layers.AveragePooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(layers.AveragePooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), padding='same', activation='relu'))
model.add(layers.AveragePooling2D((2, 2)))
# model.add(layers.Conv2D(128, (3, 3), activation='relu'))
# model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(5000, activation='relu'))
model.add(layers.Dense(200, activation='relu'))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(4, activation='softmax'))

# Consultar la arquitectura de la red
model.summary()

# Definir la pérdida, el optimizador y la métrica:
# model.compile(loss='binary_crossentropy', optimizer=optimizers.RMSprop(lr=1e-4),metrics=['acc'])
# model.compile(loss='mean_squared_error', optimizer='sgd', metrics=['acc'])

model.compile(optimizer='rmsprop', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Preprocesar los datos. Leer las imágenes (RGB), convertirlas en tensores (0 a 255) y los reescalamos a valores entre 0 y 1.

batch_size = 256
epochs = 10

# Importar librerías
# Reescalar las imágenes
train_datagen = ImageDataGenerator(
        zoom_range=0.2, 
        rotation_range=10,
        width_shift_range=0.1,
        height_shift_range=0.1,
        horizontal_flip=True,
        vertical_flip=False)
test_datagen = ImageDataGenerator(
        zoom_range=0.2, 
        rotation_range=10,
        width_shift_range=0.1,
        height_shift_range=0.1,
        horizontal_flip=True,
        vertical_flip=False)

train_generator = train_datagen.flow_from_directory(
    # r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\CNNfiles\train",
    r"C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\CNNfiles\train",
    target_size=(40, 40),
    batch_size=batch_size,
    class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
    # r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\CNNfiles\validation",
    r"C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\CNNfiles\validation",
    target_size=(40, 40),
    batch_size=batch_size,
    class_mode='binary')


test = test_datagen.flow_from_directory(
    r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\TestZone\test",
    # r"C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\CNNfiles\validation",
    target_size=(40, 40),
    batch_size=batch_size,
    class_mode='binary')

print(train_generator.class_indices)
print(validation_generator.class_indices)


# Ajustar el modelo utilizando un generador:
history = model.fit_generator(
    train_generator,
    steps_per_epoch=100,
    epochs=10,
    validation_data=validation_generator,
    validation_steps=100,
    workers=4)


predict=model.predict_generator(test, steps=10)
print(predict)


current_directory = os.getcwd()

# Guardar el modelo en formato JSON
from keras.models import model_from_json
model_json = model.to_json()
with open(current_directory+"/network_clase.json", "w") as json_file:
    json_file.write(model_json)

# Guardar el modelo:
model.save('emotion_recognition.h5')

# Visualizar la pérdida y precisión del ajuste del modelo después del proceso de entrenamiento y validación.
acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(1, len(acc) + 1)
plt.plot(epochs, acc, 'bo', label='Precisión del Entrenamiento')
plt.plot(epochs, val_acc, 'b', label='Precisión de la Validación')
plt.title('Precisión del entrenamiento y validación')
plt.legend()
plt.figure()
plt.plot(epochs, loss, 'bo', label='Pérdida del entrenamiento')
plt.plot(epochs, val_loss, 'b', label='Pérdida de la validacion')
plt.title('Pérdida del entrenamiento y validación')
plt.legend()
plt.show()
