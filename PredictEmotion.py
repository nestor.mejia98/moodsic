# Modify 'test1.jpg' and 'test2.jpg' to the images you want to predict on

from keras.models import load_model
from keras.preprocessing import image
import numpy as np

# dimensions of our images
img_width, img_height = 40, 40

# load the model we saved
model = load_model(r'C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\emotion_recognition.h5')
model.compile(loss='sparse_categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

file = r"C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\TestZone\test\happy\happy.jpeg"

# predicting images
img = image.load_img(file, target_size=(img_width, img_height))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)

images = np.vstack([x])
classes = model.predict_classes(images, batch_size=256)
print("Predicting on file")
print(file)
print (classes)