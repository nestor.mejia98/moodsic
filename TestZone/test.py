#Archivo que realiza predicciones sobre imágenes sin categorizar
import numpy as np
import matplotlib.image as mpimg

from keras.models import model_from_json
import os

current_directory = os.getcwd()

# Leer JSON y crear el modelo
json_file = open(r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\network_clase.json", "r")
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

# Cargar los pesos (weights) en un nuevo modelo
loaded_model.load_weights(r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\emotion_recognition.h5")
print("Modelo cargado desde el disco")

files_directory = r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\TestZone\imagesTest"

array_of_files = np.array([])
paths = np.array([])

#Rellenamos el array con las imagenes

for path in os.listdir(files_directory):
    full_path = os.path.join(files_directory, path)
    #Convertir la imagen en 2d
    image = mpimg.imread(full_path)[:,:,0]
    array_of_files = np.append(array_of_files, [image])
    paths = np.append(paths, [full_path])
    
#Redimensionar
array_of_files = array_of_files.reshape(1, 40,40,4)
array_of_files = array_of_files.astype('float32') / 255

#Predecir
my_prediction = loaded_model.predict_classes(array_of_files)