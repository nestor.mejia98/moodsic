from resources.trimFace import *
from resources.getImagesFromJson import *
from resources.ListFiles import *
from resources.resizeImages import *
from resources.getLabelsFromJson import *


# for file in local_files:
#         filenameExt = file.split("\\")[10]
#         filename = filenameExt.split(".")[0]
#         reshapeImage(file, filename)

images = getImages()


data = getLabels()

file='json_labels.txt'
form="%s\n"
with open(file,'w') as f:
    for i in range(len(data)):
        f.write(form % (data[i]))



for file in local_files:
    filenameExt = file.split("\\")[10]
    filename = filenameExt.split(".")[0]
    reshapeImage(file, filename)


# path = "https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"
# isURL = True
# trimFace.getFace(path, 260, 170, 416, 333)
# C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\images\json_dataset\7.jpg
# c:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\images\json_dataset\7.jpg
