import cv2
 
img = cv2.imread(r'C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\cutImages\20_2.png', cv2.IMREAD_UNCHANGED)
 
print('Original Dimensions : ',img.shape)
 
width = 40
height = 40
dim = (width, height)

# resize image
resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

print('Resized Dimensions : ', resized.shape)

cv2.imshow("Resized image", resized)
cv2.imwrite("cutImages/cut.png".format(), resized)
cv2.waitKey(0)
cv2.destroyAllWindows()
