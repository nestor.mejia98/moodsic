import os
import re
import shutil
import ntpath

pathSource = r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\resizedImages\JsonDataset"


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    return [atoi(c) for c in re.split(r'(\d+)', text)]


def listFiles(path):
    files_directory = path
    sortedListFiels = os.listdir(files_directory)

    sortedListFiels.sort(key=natural_keys)

    files = []

    for path in sortedListFiels:
        files.append(os.path.join(files_directory, path))

    return files


def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def switch(lavel, path):
    happyFolder = "C:/Users/SERGIO/Documents/SANP/UEA/2019-2020/moodsic/resizedImages/facesByEmotion/happy/"
    angryFolder = "C:/Users/SERGIO/Documents/SANP/UEA/2019-2020/moodsic/resizedImages/facesByEmotion/angry/"
    neutralFolder = "C:/Users/SERGIO/Documents/SANP/UEA/2019-2020/moodsic/resizedImages/facesByEmotion/neutral/"
    sadFolder = "C:/Users/SERGIO/Documents/SANP/UEA/2019-2020/moodsic/resizedImages/facesByEmotion/sad/"
    if('Emotion_Happy\n' == lavel):
        shutil.move(path, happyFolder + path_leaf(path))
    elif ('Emotion_Neutral\n' == lavel):
        shutil.move(path,  neutralFolder + path_leaf(path))
    elif('Emotion_Sad\n' == lavel):
        shutil.move(path, sadFolder + path_leaf(path))
    elif('Emotion_Angry\n' == lavel):
        shutil.move(path, angryFolder + path_leaf(path))
    else:
        print("invalidEmotion")


def mergeImages(fileList, lavelsList):
    i = 0
    for lavel in lavelsList:
        print("se mueve el file: " + str(i))
        print(lavel)
        switch(lavel, fileList[i])
        i += 1


listF = listFiles(pathSource)
fileName = r"C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\json_labels.txt"

with open(fileName) as f:
    lavelsList = f.readlines()

# listFilesName = [path_leaf(listF) for path in listF]

mergeImages(listF, lavelsList)
