import pandas as pd
import os
import math

def getImages():

    file = r'C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\resources\Face_Dataset_With_Emotion_Age_Ethnicity.json'
    # file = r'C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\resources\Face_Dataset_With_Emotion_Age_Ethnicity.json'
    data = pd.read_json(file)

    urls = []

    for image in data:
        obj = {}
        obj['url'] = data[image].content

        faces_points = []
        faces = data[image].annotation

        for face in faces:

        
            if face["label"][0] != "Not_Face":

                image_width = face['imageWidth']
                image_height = face['imageHeight']

                rectangle = {}
                rectangle['start'] = {}
                rectangle['start']['x'] = math.floor(face['points'][0]['x'] * image_width)
                rectangle['start']['y'] = math.floor(face['points'][0]['y'] * image_height)
                rectangle['finish'] = {}
                rectangle['finish']['x'] = math.floor(face['points'][1]['x'] * image_width)
                rectangle['finish']['y'] = math.floor(face['points'][1]['y'] * image_height)



            

                faces_points.append(rectangle)
        
        obj['faces'] = faces_points


        urls.append(obj)

    return urls