import pandas as pd
import os
import math

def getLabels():

    file = r'C:\Users\NESTOR MEJIA\Documents\UNEATLANTICO\07_Septimo_Semestre\Inteligencia Artificial\moodsic\resources\Face_Dataset_With_Emotion_Age_Ethnicity.json'
    # file = r'C:\Users\SERGIO\Documents\SANP\UEA\2019-2020\moodsic\resources\Face_Dataset_With_Emotion_Age_Ethnicity.json'
    data = pd.read_json(file)

    labels = []

    for image in data:

        faces = data[image].annotation

        for face in faces:

            if face["label"][0] != "Not_Face":
                
                labels.append(face["label"][0])
                          

    return labels