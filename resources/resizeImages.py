import cv2
import os
import shutil


def reshapeImage(path, filename):

    img = cv2.imread(path, cv2.IMREAD_UNCHANGED)

    width = 40
    height = 40
    dim = (width, height)

    # resize image
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

    # cv2.imshow("Resized image", resized)
    cv2.imwrite("resizedImages/resized"+filename+".png".format(), resized)
