import cv2


def convertToRGB(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


def getFace(path, start_x, start_y, end_x, end_y, filename):
    image = cv2.imread(path)
    print("filename: "+filename)
    # copy = image.copy()
    # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ROI = image[start_y:end_y, start_x:end_x]
    cv2.imwrite("cutImages/"+str(filename)+'.png'.format(), ROI)
